#+TITLE: Cross interview with Valérie Orozco and Christophe Bontemps
#+Date: <déc 2019>
#+Author: Valérie Orozco et Christophe Bontemps
#+Language: en
** Biography
   :PROPERTIES:
   :CUSTOM_ID: biography
   :END:

*Valérie Orozco* has been an engineer in statistics/econometrics at the
INRAE since 2004, in addition to being a member of the Industrial
Organization group at the TSE (Toulouse School of Economics). She is
chiefly involved in research into the economy of agribusiness firms and
the links between diet and health.

*Christophe Bontemps* is an engineer in econometrics at the INRAE and a
member of the Food Economics and Policy group at the TSE (Toulouse
School of Economics). He is involved in research into the economy of
agribusiness firms, the environment and the links between diet and
health.

** Interview
   :PROPERTIES:
   :CUSTOM_ID: interview
   :END:

*** Tell us a bit about yourselves: your identity, your field of research and your scientific interests outside of your discipline (e.g. R, Python, etc.).
    :PROPERTIES:
    :CUSTOM_ID: tell-us-a-bit-about-yourselves-your-identity-your-field-of-research-and-your-scientific-interests-outside-of-your-discipline-e.g.r-python-etc..
    :END:

We work at the crossroads between two worlds: the world of *mathematics
(statistics)* and the world of the *economy*, particularly in relation
to food and the environment.

As econometrists-engineers, we collaborate on scientific projects,
acting as an interface between these two worlds. Part of this involves
interpreting highly formalised economic concepts in order to test them
in practical situations on datasets, and so we use tools from both
worlds. We tend to favour articles written in LaTeX and the use of R,
but we also use Stata, which is proprietary software that is absolutely
essential in econometrics.

*** How do you handle the issue of reproducibility and transparency in your field of research?
    :PROPERTIES:
    :CUSTOM_ID: how-do-you-handle-the-issue-of-reproducibility-and-transparency-in-your-field-of-research
    :END:

The research we are involved in has *a great deal of empiricism**. Most
often, a problem is observed and raises questions. The behaviour of
agents (companies, consumers, decision-makers) is modelled based on
economic theory, and then a hypothesis is put forward to explain the
observed phenomenon.

This hypothesis is then confronted with the data. If the hypothesis can
be verified on a relevant dataset, then recommendations can be made and
we get a result. For us, "results" correspond to all of the elements
forming the response to the research question and are often based on
empirical results (in the sense of outputs): descriptive statistics,
estimates of economic models

Unlike other disciplines, *we don't put together experiments for
collecting data or for replicating phenomena*. We use *observed data*
(household purchases, for example) on which *we test hypotheses* and
*estimate the parameters of the economic model* (e.g. price elasticity).

The reproducibility of results can be expressed either as the capacity
to reproduce all of the results based on the original data, or as the
possibility of finding the same type of results on another dataset.

In the first instance, the processing chain ensures that the result is
reproducible, in the sense of quality control; in the second it ensures
that the result is transposable and generalisable to other situations.

The term "robust" refers more to this second instance. Furthermore, for
a given research question, we often test the robustness of the results
obtained, either by testing the model on another dataset, or by
modifying one of the model's hypotheses.

*** Which research practices do you employ in order to boost reproducibility and transparency? (when working alone, when working as part of a group)
    :PROPERTIES:
    :CUSTOM_ID: which-research-practices-do-you-employ-in-order-to-boost-reproducibility-and-transparency-when-working-alone-when-working-as-part-of-a-group
    :END:

The most important thing for us is *understanding the sequence of tasks
and programs (the workflow)*. Obviously, this is easier when we are
fully in charge of the code and the data (meaning that, even when we are
collaborating with other researchers, we are "responsible" for the code
and the data).

We use simple tools and a structure involving master scripts and
sub-programs. We also use makefiles and workflow representation tools
such as Graphviz. Obviously, if the project and the researcher lend
themselves to it, we use R (and notebooks/Markdown) and git. For writing
articles, we favour Overleaf for collaborative writing and Zotero (or
bibTex/JabRef) for sharing bibliographies.

*** Which tools do you use?
    :PROPERTIES:
    :CUSTOM_ID: which-tools-do-you-use
    :END:

Although we point out the benefits of free software to our researchers,
we can't change their practices out of the blue.

*In economics, there are many who use proprietary software* (Stata, SAS,
Matlab, Mathematica, Gams, etc.), although the use of R is becoming more
widespread.

*Even though they hinder reuse by someone else, we don't feel that using
these programs stands in the way of carrying out reproducible research,
provided best practice is followed*.

When we use a program such as Stata, there is nothing stopping us from
distributing a readable, structured code in which all of the stages and
all of the results can be found. Naturally, a Stata license would be
needed in order to use this code, but it is not necessarily overly
complicated to understand or to read since the code is in text format.

*** Do you use notebooks or markup tools?
    :PROPERTIES:
    :CUSTOM_ID: do-you-use-notebooks-or-markup-tools
    :END:

We try to *document our projects as much as possible* (data, modelling
choices, ideas we've given up on, hypotheses made, etc.) and to improve
the practices we use for exchanging information (in particular, avoiding
emails for communicating about the progress of a project).

We use *TMS (Task Management System) tools*, such as Trello, for
example.

With regard to Markdown, it quickly established itself in our habits,
particularly when it comes to generating reproducible documents
automatically from code (in R but also in Stata), but also for Jupyter
notebooks, which we have tested.

*** What are your workflows? How do you connect the different tools that you use on a daily basis? What data preparation steps (coding, documentation) do you follow?
    :PROPERTIES:
    :CUSTOM_ID: what-are-your-workflows-how-do-you-connect-the-different-tools-that-you-use-on-a-daily-basis-what-data-preparation-steps-coding-documentation-do-you-follow
    :END:

*** Which best practice in relation to modelling (packages, functions, etc.) have you learned to implement? How are you able to track the different versions of your scripts, your code, your text, etc.?
   :PROPERTIES:
   :CUSTOM_ID: which-best-practice-in-relation-to-modelling-packages-functions-etc.-have-you-learned-to-implement-how-are-you-able-to-track-the-different-versions-of-your-scripts-your-code-your-text-etc.
   :END:

It would take a whole article to describe all of that, and in fact,
we've actually written one, which will be published soon. *Data often
comes in the form of tables*, which we sometimes pair up, but the
structure of the data we use tends to be simple.

We have grown accustomed to a clear *folder structure*, separating
original data, programs, work data and documentation. We also have
*naming conventions for files and variables*, which make life easier for
us.

Then, to be frank, *we often start with a program that is a bit muddled
and which has become too long*. We then take *a piece of paper and write
the start of a workflow, with arrows*. We then re-write the program,
making it cleaner by using functions and sub-programs. After that, we
modify the workflow as the project advances, drawing it in Graphviz.

*If we're fortunate enough to be using R, git/GitHub is used to track
the different versions of programs* - if not, *versions are managed by
hand* (naming conventions, documentation within programs and data) even
if git/GitHub can also be used for Stata programs.

We are always trying to "think automation", particularly when it comes
to directly exporting all of the outputs generated (tables, graphs,
etc.) automatically for insertion into the final paper.

With regard to collective paper writing, Overleaf can be used to track
the different versions (payed version). *In the end, we rely on a lot of
common sense, some organisation and a few useful tools* which we find by
keeping our minds open to new practices - and closed to spreadsheet type
tools, MS-Word, etc., which are still used widely in economics.

*** What has that changed in terms of how you publish?
   :PROPERTIES:
   :CUSTOM_ID: what-has-that-changed-in-terms-of-how-you-publish
   :END:

More than anything else, it has changed *how we work with researchers*.
By repeatedly showing us examples - and dramatic counterexamples - some
researchers have left us in charge of the empirical work.

What a joy it is to launch a program and to see the screen flickering,
to see graphics gradually begin to appear and lines start to pass across
the console, before eventually finding all of the outputs properly
arranged in their folders!

Our practices have evolved over time, through our use of new tools and
through developing an understanding of the entire workflow. In the end,
it's experiments with tools of increasing performance and utility, plus
common sense and a constant desire to improve.

*** When did you develop an interest in this issue? What advice would you give to young students? (and those not so young)
    :PROPERTIES:
    :CUSTOM_ID: when-did-you-develop-an-interest-in-this-issue-what-advice-would-you-give-to-young-students-and-those-not-so-young
    :END:

*** What advice would you give in terms of training and support for researchers interested in making their research more transparent? What challenges are currently facing your field?
    :PROPERTIES:
    :CUSTOM_ID: what-advice-would-you-give-in-terms-of-training-and-support-for-researchers-interested-in-making-their-research-more-transparent-what-challenges-are-currently-facing-your-field
    :END:

Relatively early, in 2005. We were using data which we bought regularly
(once a year), for which new versions could be sent to us. *This meant
it was necessary to think automation straight away*. Working as 2
engineers on this data, we had to think about reproducibility on our 2
PCs and then on any other.

It was a gradual process. Sharing our practices, comparing our ideas,
and the evolution of tools were all real driving forces. The arrival of
Sweave in 2008, strongly advocated by J. Racine during a visit he made
to our laboratory, was another trigger.

The training given to researchers has to be improved - particularly for
young people - if possible during their studies. Obviously, we can now
cite the MOOC, as well as a number of papers (Gentzkrow and Shapiro
2014, Stodden 2013, etc.).

Journals also have a vital role to play, by increasing the requirements
with regard to verification and reproducibility on the basis of the data
and code provided (with expert reviewers or by calling on third parties
such as CASCAD), and by providing the resources needed for making
progress (proposing an repository specific to the journal or suggesting
an external one).

Obviously, confidentiality data remains a critical issue, but with good
metadata, a DOI and the recent arrival of CASCAD, a
[[https://www.cascad.tech/][reproducibility certification platform]],
reproducibility is improving for this case as well.

*** Could you tell us about any horror stories you've had? i.e. a blown opportunity (e.g. data not available; not being able to reproduce a figure and obtain the same result, not being able to supply a referee with data, etc.)
    :PROPERTIES:
    :CUSTOM_ID: could-you-tell-us-about-any-horror-stories-youve-had-i.e.a-blown-opportunity-e.g.data-not-available-not-being-able-to-reproduce-a-figure-and-obtain-the-same-result-not-being-able-to-supply-a-referee-with-data-etc.
    :END:

*We're living in a horror movie!* With some rare exceptions, "our"
researchers are not programmers, and don't (yet) have much encouragement
to publish and distribute their programs.

Furthermore, given that our data is very often confidential, journals,
with some very rare exceptions, don't ask to test programs, not even on
fictitious data.

It can be an absolute nightmare when a referee asks for an estimate to
be redone and the researcher is unable to obtain the same result and is
not entirely sure of the programs used.

It is also worth noting that *econometric estimates are often made by
maximising a likelihood*, a function that is often complex to maximise,
and it can happen that a software version change (Stata, for example,
but an update to an R package or an obsolete R package can also have
some surprises in store) disturbs the delicate mechanism that has been
developed.

*** Could you give us some concrete examples of practices aimed at making your research reproducible and transparent which have helped you to solve a problem?
    :PROPERTIES:
    :CUSTOM_ID: could-you-give-us-some-concrete-examples-of-practices-aimed-at-making-your-research-reproducible-and-transparent-which-have-helped-you-to-solve-a-problem
    :END:

*We were probably doing reproducible research without knowing it when we
first started out in 2003*. For a research program on the growth of
private labels, we had to replicate an econometric analysis carried out
on one category of product (yoghurts) on several others.

We began by setting up a workflow, then automatic outputs in LaTeX,
before finally building a "mixer" which would take any food product and
automatically produce a PDF document (via LaTeX) with the full
econometric analysis. The whole thing was programmed using Stata - it
was a real achievement!

This process enabled us to consider writing a first article, followed by
a second one that was more complex, relating to more than a hundred
products and designed to demonstrate the impact of the introduction of
private labels on the prices of national brands. This was a really
enjoyable project, which helped establish our way of working from then
on.

Another very positive example comes in the form of a recent paper to be
published: *to have anticipated the journal's request to supply the code and data and to thus have been capable of providing them in 2 days (clean, annotated codes, data, a documented workflow, one single program
to launch generating a webpage with all of the paper's outputs)*.

*** If you could add a question to this interview, what would it be? What would you like to talk about?
    :PROPERTIES:
    :CUSTOM_ID: if-you-could-add-a-question-to-this-interview-what-would-it-be-what-would-you-like-to-talk-about
    :END:

It is *never too late to change your practices* and *to organise
yourself differently*. We are often criticised for trying to complicate
a well-established, proven structure (non-reproducibility).

*To speak in economic terms, there are some for whom the costs seem to
vastly outweigh the benefits. We don't feel that to be true.* Obviously,
every change comes at a cost, particularly short-term, but we remain
convinced that *both the individual and collective benefits are
substantial and that this cost can be minimal*.

Hopefully economists will pick up on what I'm saying... However, in
order for that to work, we feel it is important *to encourage each improvement and to not try to modify all of the practices at the same
time*. Some tools are simpler than others - everything depends on where
you're starting from.
