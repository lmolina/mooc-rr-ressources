#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: Un aperçu historique de la prise de notes

#+DATE: <2019-02-21 jeu.>
#+AUTHOR: Christophe Pouzat
#+EMAIL: christophe.pouzat@parisdescartes.fr
#+LANGUAGE: fr
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.1 (Org mode 9.1.9)
#+STARTUP: indent

* Table des matières                                           :TOC:
- [[#références-générales][Références générales]]
- [[#sur-les-tablettes-de-cires][Sur les tablettes de cires]]
- [[#sur-le-passage-du-rouleau-volumen-au-codex][Sur le passage du rouleau (/volumen/) au codex]]
- [[#sur-eusèbe-de-césarée][Sur Eusèbe de Césarée]]
- [[#parallèle-chinois][Parallèle chinois]]
- [[#retour-sur-larmoire-à-notes][Retour sur l'armoire à notes]]
- [[#lindex-et-john-locke][L'index et John Locke]]

* Références générales
En plus des deux livres déjà cités (en séquence 1) :
- « La page. De l'Antiquité à l'ère du numérique » d'Anthony Grafton (Hazan, 2012) ;
- « [[https://yalebooks.yale.edu/book/9780300165395/too-much-know][TOO MUCH TO KNOW. Managing Scholarly Information before the Modern Age]] », d'Ann Blair publié chez /Yale University Press/ en 2011 ;
j'ai utilisé :
- « L'histoire du livre » de Frédéric Barbier ;
- le remarquable site de Jacques Poitou, [[http://j.poitou.free.fr/pro/index.html][langages écritures typographies]] ;
- le site [[http://classes.bnf.fr/ecritures/][l'aventure des écritures]] de la BNF ;
- le catalogue de l'exposition de la BNF « [[http://editions.bnf.fr/tous-les-savoirs-du-monde-encyclop%C3%A9dies-et-biblioth%C3%A8ques-de-sumer-au-xxie-si%C3%A8cle][Tous les savoirs du monde : Encyclopédies et bibliothèques de Sumer au XXIe siècle, sous la direction de Roland Schaer, 1997]]» ;
- « [[http://litmedmod.ca/sites/default/files/pdf/vandendorpe-papyrusenligne_lr.pdf][Du papyrus à l'hypertexte]] » de Christian Vandendorpe (La Découverte, 1999).

* Sur les tablettes de cires
Voir le site de Jacques Poitou d'où les illustrations sont empruntées([[http://j.poitou.free.fr/pro/index.html][langages écritures typographies]]) et le livre de Frédéric Barbier (« L'histoire du livre » de Frédéric Barbier - Armand Colin, 2001).
* Sur le passage du rouleau (/volumen/) au codex
Voir le livre de Frédéric Barbier (« L'histoire du livre » de Frédéric Barbier - Armand Colin, 2001), celui d'Anthony Grafton (« La page. De l'Antiquité à l'ère du numérique » de Anthony Grafton - Hazan, 2012).

Le /volumen/ est un livre à base de feuilles de papyrus collées les unes aux autres et qui s'enroule sur lui-même. Il a été créé en Égypte vers 3000 av. J.-C. Le texte est rédigé en colonnes parallèles assez étroites. C'est le support du texte par excellence durant les trente siècles précédant notre ère, d'abord en Égypte, puis dans tout le monde méditerranéen. 

Comme l'explique Frédéric Barbier : « La forme du /volumen/ impose une pratique de lecture complexe : il faut dérouler (/explicare/) et enrouler en même temps, ce qui interdit par exemple, de travailler simultanément sur plusieurs rouleaux (un texte et son commentaire) ou de prendre des notes, impose une lecture suivie et empêche la simple consultation. »

Le /volumen/ n'est clairement pas adapté à une lecture « nomade » ; imagine-t-on Ulysse partant pour son Odyssée avec les 24 /volumen/ de l'Iliade ?

Le /volumen/ est à l'origine du terme « volume » dans un « livre en plusieurs volumes » comme dans la désignation du concept géométrique.

Le passage au codex repose sur deux innovations : 
- la collection des tablettes de cires en « groupes reliés » ;
- la généralisation du parchemin (peau, généralement, de mouton spécialement préparée) au détriment du papyrus. Cette généralisation résulte une lutte pour l'hégémonie culturelle entre deux descendants de généraux d'Alexandre le Grand, en effet d'après Pline l'ancien : [[https://fr.wikipedia.org/wiki/Ptol%C3%A9m%C3%A9e_V][Ptolémé Épiphane]] d'Alexandrie cherchait à empêcher [[https://fr.wikipedia.org/wiki/Eum%C3%A8ne_II][Eumène II]] d'établir une bibliothèque à Pergame (au 2e siècle avant J.-C.) et avait interdit l'exportation du papyrus (produit exclusivement en Égypte), ce qui incita Eumène à chercher un substitut qui devint le parchemin.

Le remplacement du rouleau par le codex aura des conséquences majeures sur l'organisation du livre ainsi que sur la façon de lire et il permettra le développement ultérieur de l'imprimerie. 

La principale révolution introduite par le codex est la notion de page. Grâce à elle, le lecteur peut accéder de manière directe à un chapitre ou à un passage du texte, alors que le rouleau impose une lecture continue. *Les mots ne sont de plus pas séparés par des espaces*. Comme l'écrit Collette Sirat : « Il faudra vingt siècles pour qu’on se rende compte que l’importance primordiale du codex pour notre civilisation a été de permettre la lecture sélective et non pas continue, contribuant ainsi à l’élaboration de structures mentales où le texte est dissocié de la parole et de son rythme. »

Au fil des siècles, le codex — qu'on désigne le plus souvent comme un manuscrit — va évoluer et se donner peu à peu les attributs du livre moderne : séparation entre les mots (VIIe siècle), début de ponctuation (VIIIe siècle), table des matières, titre courant, marque de paragraphe (XIe siècle), pagination, index (XIIIe siècle), etc. 

Un point intéressant : le contenu de la Thora est « fixé » avant l'apparition du codex et, aujourd'hui encore, la Thora est écrite sur des /volumen/ (dans les synagogues au moins). La religion chrétienne se développe en même temps que le codex, adopte ce support et le répand ; elle ne donnera jamais au /volumen/ un statut « supérieur », pas plus que ne le fera la religion musulmane.

* Sur Eusèbe de Césarée
Pour en savoir plus sur [[https://fr.wikipedia.org/wiki/Eus%C3%A8be_de_C%C3%A9sar%C3%A9e][Eusèbe de Césarée]], consultez le passionnant deuxième chapitre du livre d'Anthony Grafton.
La notion de « références croisées » apparaît aussi dans un autre contexte que celui du « canon eusébien » lorsque l’œuvre d’Eusèbe de Césarée est discutée. Eusèbe de Césarée a en effet rédigé l’[[https://fr.wikipedia.org/wiki/Histoire_eccl%C3%A9siastique ][Histoire ecclésiastique]] (IVe siècle après J.-C.) et a employé un « recoupement de sources » (comparaison de textes de différents auteurs relatifs au même événement) pour étayer sa chronologie – voir l’article de Claire Muckenstum-Poulle « [[https://www.persee.fr/doc/dha_2108-1433_2010_sup_4_1_3347?q=Muckensturm-Poulle][Exégèse et Histoire sainte dans l’Histoire ecclésiastique d’Eusèbe de Césarée (Livre I)] », Dialogues d’histoire ancienne, 2010, Suppl. 4-1, pp. 141-153 – or il n’est pas le premier à avoir utilisé ce type de recoupements, qualifiés de « références croisées » par Claire Muckenstum-Poulle (voir les deux derniers paragraphes de la page 150) : l’historien [[https://fr.wikipedia.org/wiki/Flavius_Jos%C3%A8phe][Flavius Josèphe]] (Ier siècle après J.-C.) l’avait fait avant lui.

Par la suite, Ammonius d’Alexandrie (IIIe siècle après J.-C.) a inventé un procédé consistant à reproduire intégralement les textes des quatre évangiles dans quatre colonnes, en établissant des correspondances entre eux.

Au début du IVe siècle après J.-C., Eusèbe de Césarée reprend cette idée sous forme d’un tableau simplifié, ce qui constitue le canon eusébien. Il semble que ce tableau simplifié – dont le but est de permettre un recoupement aisé des quatre évangiles mais aurait pu être étendu à d’autres sources – soit le premier exemple de ce que nous qualifions aujourd’hui communément de « références croisées ». Je remercie M.-G. Dondon pour l’intéressante discussion à l’origine de ces clarifications et sa contribution à l’écriture des ces notes.
* Parallèle chinois
Comme je le dis, mon inculture fait que je ne rends pas justice aux contributions chinoises, musulmanes, précolombienne, etc. J'essaierai de combler cette énorme lacune pour les seconde version du CLOM...

Ce que je dis sur le passage du volumen au codex accompagné d'un développement des « outils de navigation » (index, table des matières, etc)  en Chine lors du développement de leishus vient du bouquin d'Ann Blair (p. 31) qui cite un article de Susan Cherniack, « Book Culture and Textual Transmission in Sung China », /Harvard Journal of Asiatic Studies/ Vol. 54, No. 1 (Jun., 1994), pp. 5-125.
* Retour sur l'armoire à notes
Nous revenons sur le « bout de papier » ou la fiche comme support de note. L'inconvénient est que le bout de papier ou la fiche se perdent facilement et ne servent à rien s'ils ne sont pas *classés* en plus d'être rangés. Problème résolu par l'armoire de Placcius. D'une certaine façon, sa conception fait qu'on accède à son contenu par l'index.

L'avantage est que les notes peuvent être réorganisées si elles contiennent des information sur plusieurs sujets. Elle peuvent aussi être directement collées dans un livre lors de la composition d'un florilège ou d'un ouvrage de synthèse. 

Ce dernier procédé était très couramment employé par les humanistes et les érudit de la renaissance et du début de la période moderne. [[https://fr.wikipedia.org/wiki/Conrad_Gessner][Conrad Gessner]] (1516-1565) était un champion de cette technique ; il obtenait même parfois ses fiches en découpant les pages des livres. Encore une fois, ne faites pas cela avec les livres de bibliothèques !

* L'index et John Locke  

Sur l'origine de l'index, on pourra lire l'article de Jean Berger : [[https://www.theindexer.org/files/25-2-berger.pdf][Indexation, Memoire, pouvoir et representations au seuil du XIIe siecle : La redecouverte des feuillets de tables du Liber De Honoribus, premier cartulaire de la collegiale Saint-Julien de Brioude]], /The Indexer/.

La méthode de John Lock est expliquée dans l'article /Indexing commonplace books: John Locke’s method/ d'Alan Walker, [[https://www.theindexer.org/issues/query.php?vol=22&iss=3][The Indexer]], vol. 22, p. 114-118, 2001.

Une précision manque dans mes propos de la vidéo : la deuxième lettre (la voyelle) qui constitue l’entrée d’un mot clé dans l’index est la première voyelle qui suit la première lettre. Le mot clé « Analyse » trouverait donc sa place à la ligne « Aa » de l’index.

Sur [[https://fr.wikipedia.org/wiki/John_Locke][John Locke]] (1632-1704) « papa du libéralisme » et actionnaire de la /Royal African Company/ principale compagnie négrière britannique, voir l'article [[https://en.wikipedia.org/wiki/John_Locke#Constitution_of_Carolina][Wikipedia]] (en anglais) et le livre « Contre-histoire du libéralisme » de Domenico Losurdo (La Découverte / Poche, 2014, p. 34-36).
