#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: Notes (and codes) that are archived but can evolve with version control systems
#+AUTHOR: Christophe Pouzat
#+EMAIL: christophe.pouzat@parisdescartes.fr
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.1 (Org mode 9.1.9)
#+STARTUP: indent

- [[#version-control-with-gitlab-and-git-in-the-Reproducible-research-mooc][Version control with GitLab and Git]]
- [[#version-control-with-libreoffice-or-dokuwiki][Version control with LibreOffice or DokuWiki]]
* Essai pour tester les liens relatifs
- Lien relatif [[/jump_to_id/5571950188c946e790f06d4bc90fb5f6#InterfaceGitlab][video]] shows how to use GitLab. We strongly recommend that you watch it if you've never used GitLab before.
- Autre essai lien relatif : [[./jump_to_id/5571950188c946e790f06d4bc90fb5f6#InterfaceGitlab][video]]
- Lien absolu session02 :  [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/jump_to_id/5571950188c946e790f06d4bc90fb5f6#InterfaceGitlab][video]] 
- Lien absolu session03 :  [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/jump_to_id/5571950188c946e790f06d4bc90fb5f6#InterfaceGitlab][video]]

Test avec du code html
#+BEGIN_EXPORT html
  <a href="/jump_to_id/5571950188c946e790f06d4bc90fb5f6#InterfaceGitlab"> Lien relatif avec code html dans le fic.org</a>
#+END_EXPORT
