# -*- mode: org -*-
#+TITLE:     Rstudio
#+DATE: February 2020
#+STARTUP: overview indent
#+OPTIONS: num:nil toc:t

* Table of Contents                                                     :TOC:
- [[#installing-rstudio-and-r][Installing RStudio and R]]
  - [[#latex-for-generating-pdf-files][LaTeX for generating PDF files]]
- [[#rstudio-documentation][RStudio documentation]]
- [[#using-git-from-rstudio][Using Git from RStudio]]

* Installing RStudio and R
There are many different ways to install R and RStudio on your computer. The one we recommend, and describe in the following, uses the package manager [[https://conda.io/][conda]]. It has the advantage of working more or less identically under Linux, Windows, and macOS. Another advantage is that the conda ecosystem contains a wide range of scientific software, which you might want to use for your own projects. If you are an experienced systems administrator, you are of course free to choose another approach to installing software, it's only the result that matters.

First, download the [[https://conda.io/miniconda.html][most recent version of Miniconda]]. Miniconda is a lightweight edition of Anaconda, a software distribution that includes Python, R, Jupyter, and many popular libraries for scientific computing and data science.

Install Miniconda following the supplied instructions. Whenever (it it not systematic) the installer asks you the question
#+begin_quote
Do you wish the installer to initialize Miniconda3
by running conda init? [yes|no]
#+end_quote
answer =yes=. You will then see the advice
#+begin_quote
==> For changes to take effect, close and re-open your current shell. <==
#+end_quote
which you must respect to make sure that the following steps work correctly. 

*Important:* You should then run all the following commands through the
conda shell. As explained in the [[https://docs.anaconda.com/anaconda/install/verify-install/][Anaconda documentation]], to open the Anaconda prompt:
- Windows: Click Start, search, or select Anaconda Prompt from the menu.
  [[file:Conda_images/win-anaconda-prompt.png]]
- macOS: Cmd+Space to open Spotlight Search and type “terminal” to open the program.
- Linux–CentOS: Open Applications - System Tools - terminal.
- Linux–Ubuntu: Open the Dash by clicking the upper left Ubuntu icon, then type “terminal”.

The first command to run next is
#+begin_src shell :results output :exports both
conda update -n base -c defaults conda
#+end_src
which updates all the software in the =conda= distribution.

We can now create a =conda= environment for the RStudio path of out MOOC:
#+begin_src shell :results output :exports both
conda create -n mooc-rr-rstudio
#+end_src
and activate it:
#+begin_src shell :results output :exports both
conda activate mooc-rr-rstudio
#+end_src
It is not strictly necessary to activate an environment in order to use it, but doing so makes the use of the environment easier and less error prone. *You have to perform this activation step every time you open a new terminal, before you can work with the environment.*

The next step is the installation of all software packages we need and which are in the Miniconda distribution:
#+begin_src shell :results output :exports both
conda install rstudio r r-ggplot2 r-reticulate python
#+end_src
We also need one package that is not in Miniconda, so we request it from the independent package source [[https://conda-forge.org/][conda-forge]]:
#+begin_src shell :results output :exports both
conda install -c conda-forge r-parsedate
#+end_src

You can now start RStudio directly from the command line:
#+begin_src shell :results output :exports both
rstudio
#+end_src

Under Windows and macOS, you might be tempted to start RStudio from the graphical user interface. It is in fact not so difficult to find =RStudio.app= in the =conda= environment and open it directly. But you have to resist that temptation. If you don't run RStudio from the command line, it won't be able to detect the conda environment that you activated.
** LaTeX for generating PDF files
If you want to convert your notebooks to PDF files, you must als install LaTeX on your computer. We explain this step in a [[file:latex.org][separate resource]].
* RStudio documentation
The RStudio team has created a lot of very good material and
tutorials. You should definitively look at the [[https://www.rstudio.com/resources/cheatsheets/][Cheat sheets
webpage]]. In particular you may want to have look at the following
ones:
- [[https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf][The RStudio IDE]],
- [[https://github.com/rstudio/cheatsheets/raw/master/rmarkdown-2.0.pdf][R Markdown]] (here is also a [[https://rmarkdown.rstudio.com/][nice step-by-step presentation of Rmarkdown]]),
- The [[https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf][R Markdown Reference guide]],
- [[https://github.com/rstudio/cheatsheets/raw/master/data-visualization-2.1.pdf][Data visualization with ggplot2]],
- [[https://github.com/rstudio/cheatsheets/raw/master/data-transformation.pdf][Data transformation with dplyr]]
In case it helps, here are some (sometimes outdated) French versions
of these documents: 
- [[https://github.com/rstudio/cheatsheets/raw/master/translations/french/rstudio-IDE-cheatsheet.pdf][L'IDE RStudio]]
- [[https://github.com/rstudio/cheatsheets/raw/master/translations/french/ggplot2-french-cheatsheet.pdf][Visualisation de données avec ggplot2]]
- [[https://github.com/rstudio/cheatsheets/raw/master/translations/french/data-wrangling-french.pdf][Transformation de données avec dplyr]]
- [[https://www.fun-mooc.fr/c4x/UPSUD/42001S02/asset/RMarkdown.pdf][Un court document sur R Markdown]]

* Using Git from RStudio
This part is the subject of a [[https://gitlab.inria.fr/learninglab/mooc-rr/mooc-rr-ressources/-/blob/master/module2/ressources/rstudio-git.org][specific resource]].