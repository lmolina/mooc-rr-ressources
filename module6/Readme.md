# Ressources du futur Mooc "Recherche reproductible II" / Resources of the future Mooc "Reproducible Research II"

[English version below]

Cet entrepôt contiendra des ressources du Mooc "Ressources du Mooc Recherche reproductible II : pratiques et outils pour gérer calculs et données".

**Attention, ce Mooc est en cours de production et les ressources ne sont pas finalisées ni pérennes**.

-----------------------------------------------------------------
This repository will contain some resources of the Mooc "Reproducible research II: Recherche reproductible II : practices and tools to manage calculations".

**Be careful, this Mooc is in production and the resources are not finalized nor permanent**.
