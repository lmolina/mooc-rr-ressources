\documentclass{beamer}

\usetheme{Madrid}
\usecolortheme{crane}

\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{textpos}
\usepackage{multirow}
\usepackage{graphicx}

\definecolor{darkgreen}{rgb}{0.0, 0.3, 0.13}

\newcommand{\bluehref}[2]{\href{#1}{\textcolor{blue}{#2}}}

\definecolor{emph}{RGB}{10,150,10}
\definecolor{contrast}{RGB}{0,0,120}
\definecolor{problem}{RGB}{120,0,0}
\definecolor{nice}{RGB}{10,150,10}

\usepackage{listings,bera}
\definecolor{sourcecode}{RGB}{0,0,100}
\definecolor{keywords}{RGB}{200,90,0}
\definecolor{comments}{RGB}{60,179,113}


\title{Automating computations: an introduction to workflows}
\author[Konrad HINSEN]{Konrad HINSEN}
\institute[CBM/SOLEIL]
          {Centre de Biophysique Moléculaire, Orléans, France\\
           and\\
           Synchrotron SOLEIL, Saint Aubin, France}
\date{}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Automation matters}

  \begin{itemize}
  \item Interactive work is great for exploration, but rarely reproducible.
  \item Even if you write down everything you do, you will make mistakes.
  \item And even if you don't make mistakes, you are never \textit{sure}
        there are no mistakes.
  \item \textbf{You know exactly what you have done only if you can re-run your computation.}
  \end{itemize}

\end{frame}

\begin{frame}{Programs, scripts, notebooks, workflows}

  \begin{itemize}
  \item Variants of the same idea: automation of computations
  \item Different terms and tools at different levels of organization
  \item Program: complex algorithms and/or data structures
  \item Script: short program, simple algorithms, no data abstraction
  \item Notebook: script with comments and output
  \item Workflow: coordination of multiple programs, multiple computers, multiple datasets
  \end{itemize}

\end{frame}

\begin{frame}{Overview}

  \begin{enumerate}
  \item Why workflows?
  \item How to choose a workflow manager?
  \item The descendants of \texttt{Make}
  \item Snakemake: Make for scientific computing
  \item Case study: the incidence of influenza-like illness revisited
  \end{enumerate}

\end{frame}

\section*{Why workflows?}

\begin{frame}{}

  \begin{center}
  \Large Why workflows?
  \end{center}

\end{frame}

\begin{frame}{The limits of scripts and notebooks}

  \begin{itemize}
  \item Linear control flow: hard to read when too long
  \item Not well adapted to parameter scans and other loops
  \item Linear list of results: hard to inspect when too numerous
  \item All or nothing: no minimal update after change of an input
  \item Parallelization opportunities difficult to exploit
  \end{itemize}

\end{frame}

\begin{frame}{Tasks: the building blocks of workflows}

  \begin{itemize}
  \item Task = step in a computation
  \item Code + input data $\rightarrow$ output data
  \item Often implemented as a short script (bash, Python, R, ...)
  \item Input and output data stored in files
  \end{itemize}

\end{frame}

\begin{frame}{Workflows: execution plans for tasks}

  \begin{itemize}
  \item One task's output is another task's input
  \item Simple control structures, e.g. parameter scans
  \end{itemize}

  TODO: image of a workflow graph

\end{frame}

\begin{frame}{Workflow execution}

  \begin{itemize}
  \item Run tasks one by one or in parallel ...
  \item ... on one computer or distributed among several ...
  \item ... respecting the constraints of the workflow:
    \begin{itemize}
    \item Never run a task before its input data is ready
    \item Avoid running a task if its output is already available
    \end{itemize}
  \item Deal with failures (machine crash, broken network connection, ...)
  \end{itemize}

\end{frame}

\section*{How to choose a workflow manager?}

\begin{frame}{}

  \begin{center}
  \Large How to choose a workflow manager?
  \end{center}

\end{frame}

\begin{frame}{The messy world of workflows}

  \begin{itemize}
  \item Dozens of widely used workflow managers
  \item Hundreds of lesser used ones
  \item Cover widely varying needs
  \item The idea has been rediscovered independently many times
  \end{itemize}

\end{frame}

\begin{frame}{Where are the tasks run?}

  \begin{itemize}
  \item Personal computer
  \item Remote computer with command-line access
  \item Grid, cloud
  \end{itemize}

\end{frame}

\begin{frame}{Where is the workflow managed?}

  \begin{itemize}
  \item Personal computer
  \item Server (Web interface)
  \end{itemize}

\end{frame}

\begin{frame}{Where is the data stored?}

  \begin{itemize}
  \item Local file on the machine running the task
  \item Network-accessible storage
  \item Cloud storage
  \item Web ressources
  \end{itemize}

\end{frame}

\begin{frame}{How is the computation in a task defined?}

  \begin{itemize}
  \item Compiled executable
  \item Script (Python, R, ...)
  \item Container
  \item Web service (REST API, ...)
  \end{itemize}

\end{frame}

\begin{frame}{How is the workflow defined and updated?}

  \begin{itemize}
  \item Domain-specific language
  \item General-purpose language with libraries or extensions
  \item Visual manipulation of a task graph
  \end{itemize}

\end{frame}

\begin{frame}{Which control flow features are available?}

  \begin{itemize}
  \item Data dependencies
  \item Parameter scans (loops)
  \item Dynamic task graph
  \end{itemize}

\end{frame}

\begin{frame}{How is reproducibility supported?}

  \begin{itemize}
  \item Reproducible software environments
  \item Versioning of the workflow
  \item Provenance tracking for results
  \item Provenance tracking for input data
  \end{itemize}

\end{frame}

\begin{frame}{Example: Taverna}

TODO

\end{frame}

\begin{frame}{Example: nextflow}

TODO

\end{frame}

\section*{The descendants of \texttt{Make}}

\begin{frame}{Make: workflows for building software}

  \begin{itemize}
  \item Published in 1976 for accelerating software builds
  \item Still widely used for software development
  \item Typical tasks: preprocess, compile, link
  \item Main goal: avoid unnecessary compiler runs
  \item Principle: run a task only if
    \begin{itemize}
    \item its output file does not exist OR
    \item its input file(s) are more recent than the output file
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}{Example of a Makefile}

TODO

\end{frame}

\begin{frame}{Software build tools beyond \texttt{Make}}

  \begin{itemize}
  \item GNU Build System (Make + autoconf, automake, libtool)
  \item CMake
  \item SCons
  \item Ant, Maven, Gradle
  \item Bazel
  \item ...
  \end{itemize}

\end{frame}

\begin{frame}{Software build tools in scientific computing}

  \begin{itemize}
  \item Well-known and well-documented workflow tools
  \item Designed for
    \begin{itemize}
    \item a single computer defining the software environment
    \item data in local files
    \item tasks implemented by command line tools
    \end{itemize}
  \item No support for reproducibility
  \end{itemize}

\end{frame}

\section*{Snakemake: Make for scientific computing}

\begin{frame}{}

\end{frame}

\section*{Case study: the incidence of influenza-like illness revisited}

\begin{frame}{}

\end{frame}


\end{document}
