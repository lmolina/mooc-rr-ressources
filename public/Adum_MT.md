# Champs à remplir pour les fiches dans Adum

**Nom** : Mooc Recherche reproductible : principes méthodologiques pour une science transparente.  
**Catégories** : Outils et méthodes  
**Langue de l'intervention** : bilingue français/anglais  
**Nombre d'heures** : 24h  
**Min participants** : 2  
**Max participants** : 5000 (pas de max, c'est un Mooc)  
**Public prioritaire** : Aucun  
**Public concerné** : Tout doctorant(e)s  
**Proposé par** : Inria Learning Lab  

## Fiche Description

- **Lieu** : en ligne - plateforme FUN
- **Observations** : Ce cours en ligne est accessible gratuitement sur la plateforme de Mooc FUN. L‘ensemble des contenus du Mooc est disponible dès le lancement avec une délivrance d’attestations de suivi tous les 3 mois : vous pouvez ainsi suivre le Mooc à votre rythme et selon vos besoins ! Le temps estimé pour suivre ce cours et faire les exercices est de 24h. Tous les contenus sont bilingues français / anglais.
- **Début du module** : 20 mars 2020
- **Date limite d'inscription** : 20 mars 2021
- **Modalités d'inscription** : vous devez vous même procéder à votre inscription en ligne sur la plateforme FUN https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about
- **Mots-clés** : Recherche Reproductible, Science ouverte, git, GitLab, Notebooks, Markdown, Org-mode, Jupyter, Rtudio, Python

## Objectifs : 
Vous prenez des notes et vous voulez vous y retrouver ? Vous faites des calculs sur ordinateur et vos résultats changent d’un jour à l’autre ? Vous aimeriez partager avec vos collègues vos analyses de données et vos derniers travaux et qu’ils puissent les réutiliser ? Ce Mooc est pous vous, quelque soit votre domaine.

Le cours aborde de manière pratique des thématiques telles que la prise de notes, le document computationnel, la réplicabilité des analyses.

À l’issue de ce MOOC, vous aurez acquis les techniques vous permettant de préparer des documents computationnels réplicables et de partager en toute transparence les résultats de vos travaux.

## Programme : 
Vous vous formerez à des environnements de rédaction et de publication ainsi qu'à des outils variés et fiables, pouvant répondre aux besoins de nombreuses disciplines :

- **Markdown** pour la prise de note structurée : ce langage fonctionne avec peu de balises et permet d'automatiser la production de documents de recherche notamment grâce à ses nombreux formats de sortie (PDF, HTML, Word, ...)
- des **outils d'indexation** (DocFetcher et ExifTool)
- **GitLab** pour le suivi de version et le travail collaboratif. Cet outil n'est pas réservé aux développeurs. Il facilite la révision de textes rédigés à plusieurs voire seul et au long cours : on peut par exemple connaitre les raisons qui ont poussé à apporter tel changement ou encore consulter une version antérieure du document. Dans la 3e session du Mooc, vous trouverez des vidéos sur git/GitLab pour les débutants.
- **Notebooks**  pour combiner efficacement calcul, représentation et analyse des données

Nous proposons dans ce MOOC trois parcours différents utilisant chacun une technologie de Notebook :

- Le premier parcours repose sur **Jupyter** et le langage Python (ou R). Il ne nécessite aucune installation de votre part sur votre ordinateur.
- Le deuxième parcours repose sur **RStudio** et le langage R.
- Le troisième parcours repose sur **Emacs/Org-Mode** et les langages Python et R.
Nous vous présenterons également les enjeux et les difficultés de la recherche reproductible.    Pour illustrer et approfondir certaines notions, vous pourrez écouter ou lire des interviews de nombreux chercheurs de domaines différents.

## Plan du cours :
- Posons le décor : La reproductibilité, en crise ? Reproductibilité et transparence
- Module 1 : Cahier de notes, cahier de laboratoire
- Module 2 : La vitrine et l’envers du décor : le document computationnel
- Module 3 : La main à la pâte : une analyse réplicable
- Module 4 : Vers une étude reproductible : la réalité du terrain

## Pré-requis :
Le premier module ne requiert aucune connaissance particulière. Dès le deuxième module, une connaissance des bases du langage Python (librairies pandas, numpy et matplotlib) ou du langage R est nécessaire. 
Dans le quatrième module, nous traitons des sujets plus spécialisés dont chacun peut nécessiter des compétences particulières.
Une familiarité avec l'analyse de données et les statistiques est nécessaire pour certains exercices de cette session. 
Néanmoins, même si vous ne parvenez pas à réaliser totalement ces exercices, cela ne vous empêchera pas de vous former sur de nombreux outils et méthodes pour la recherche reproductible.

## Equipe pédagogique : 
**Konrad Hinsen** est chercheur CNRS au Centre de Biophysique Moléculaire à Orléans et au Synchrotron SOLEIL à Saint Aubin. Il explore la structure et la dynamique des protéines par des méthodes computationnelles, qu’il tente de rendre reproductibles.

**Arnaud Legrand** est chercheur CNRS au Laboratoire d’Informatique de Grenoble. Il s’intéresse à l’évaluation de la performance de grandes infrastructures. Que ça soit lors de l’expérimentation ou lors de l’analyse des mesures, il est indispensable de capturer rigoureusement le processus utilisé.

**Christophe Pouzat** est chercheur CNRS au laboratoire MAP5, mathématiques appliquées à Paris-Descartes. Il est en fait neurophysiologiste et travaille sur l’analyse de données ; la recherche reproductible lui permet une communication explicite avec les expérimentateurs, ce qui évite bien des erreurs.

## Méthode pédagogique : 
Ce cours est proposé sous la forme d'un Mooc.
Ce MOOC est composé de quatre modules qui combinent des vidéos de cours, de nombreuses ressources notamment sur l'installation et l'utilisation des outils présentés (sous forme de vidéos ou de pages web), des quizz, et des exercices pour la mise en pratique des méthodes présentées.

Les exercices sont basés sur des cas concrets, permettent aux apprenants d’être en situation de recherche. 
Pour réaliser ces projets, nous proposons trois parcours, chacun utilisant une technologie différente pour écrire des documents computationnels (Jupyter, Rstudio ou Emacs/Org-mode). Un de ces parcours, Jupyter, ne nécessite aucune installation de la part de l'élève car chacun dispose d'un espace Jupyter intégré à FUN. 

De plus, pour favoriser les aspects collaboratifs de la formation, un espace personnalisé et accessible à tous a été mis en place pour chaque élève via l’outil Gitlab. 

## Pièces a fournir :
À la fin de la formation, vous devrez télécharger l'attestation de réussite sur votre espace personnel ADUM pour que nous prenions en compte ces heures de formation. 

**Site web** :
https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about
