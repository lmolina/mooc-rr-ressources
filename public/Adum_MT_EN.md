# Fields to fill in for Adum records

**Name**: MOOC Reproducible Research: methodological principles for transparent science.  
**Categories**: Tools and Methods  
**Language of intervention**: bilingual French/English  
**Number of hours** : 24 hours  
**Min participants** : 2  
**Max participants** : 5000 (no max, it's a Mooc)  
**Priority public**: None  
**Public concerned** : All doctoral students  
**Proposed by**: Inria Learning Lab  

## Description

- **Place** : online - FUN platform
- **Observations**: This online course is available for free on the FUN platform. All the contents of the MOOC are accessible from the start. Certificates of attendance are issued every 3 months: you can follow the MOOC at your own pace and according to your needs! The estimated time to follow the course and do the exercises is 24 hours. All contents are bilingual French / English.
- **Beginning of the module**: March 20, 2020
- **Last registration**: March 20, 2021
- **Registration modalities**: you must register yourself online on the FUN platform https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about
- **Keywords** : Reproducible Search, Open Science, git, GitLab, Notebooks, Markdown, Org-mode, Jupyter, Rtudio, Python

## Objectives: 
You take notes and you want to find your way around them? You do calculations on the computer and your results change from one day to the next? You Would like to share your data analysis and your latest work with your colleagues so that they can reuse it? This MOOC is for you, whatever your field.

The course covers practical topics such as note-taking, computational documents, and the replicability of analyses.

At the end of this MOOC, you will have acquired the techniques that allow you to prepare replicable computational documents and to share the results of your work transparently.

## Program : 
You will be trained in writing and publishing environments as well as in a variety of reliable tools that can meet the needs of many disciplines:

- **Markdown** for structured note-taking: this language works with few tags and allows you to automate the production of research documents thanks to its numerous output formats (PDF, HTML, Word, ...).
- **Indexing tools** (DocFetcher and ExifTool)
- **GitLab** for version tracking and collaborative work. This tool is not reserved to software developers. It facilitates the revision of texts written by several people, or even alone and over a long period of time: for example, you can know the reasons that led to a change or consult a previous version of the document. In the 3rd session of the MOOC, you will find videos on git/GitLab for beginners.
- **Notebooks** to efficiently combine calculation, representation and analysis of data

We propose in this MOOC three different paths using diffeent Notebook technology:

- The first path is based on **Jupyter** and the Python language. It does not require any installation on your computer.
- The second path is based on **RStudio** and the R language.
- The third path is based on **Emacs/Org-Mode** and the Python and R languages.
We will also explain the importance and difficulties of reproducible research.  To illustrate and explore in depth certain concepts, we propose interviews with many researchers from different fields.

## Course Outline :
- Let's set the scene : Reproducibility in crisis? Reproducibility and transparency
- Module 1: Taking notes and finding them back
- Module 2: From the showcase to the full story: computational documents
- Module 3: Diving in: a replicable analysis
- Module 4: The rough road to real-life reproducible research

## Prerequisites:
The first module does not require any special knowledge. Starting with the second module, a knowledge of the basics of the Python language (pandas, numpy and matplotlib libraries) or the R language is required. 
In the fourth module, we deal with more specialized topics, each of which may require special skills.
Familiarity with data analysis and statistics is required for some of the exercises in this session.  However, even if you are not able to fully complete these exercises, this will not prevent you from learning about the tools and methods for replicable research.

## Pedagogical team : 
**Konrad Hinsen** is a CNRS researcher at the Centre de Biophysique Moléculaire in Orléans and at the Synchrotron SOLEIL in Saint Aubin. He explores the structure and dynamics of proteins by computational methods, which he tries to make reproducible. 

**Arnaud Legrand** is a CNRS researcher at the Laboratoire d'Informatique in Grenoble. His research interest is the evaluation of the performance of big computing infrastructures. Both for performing experiments and for analyzing the outcomes, it is essential to capture the process rigorously. 

**Christophe Pouzat** is a CNRS researcher in the laboratory MAP5 (applied mathematics at Paris-Descartes). He is actually a neurophysiologist, working on the analysis of experimental data. Reproducible research enables him to communicate explicitly with experimentalists, avoiding many mistakes.

## Teaching method: 
This course is offered in the form of a MOOC.
This MOOC is composed of four modules that combine videos of the course, numerous resources notably on the installation and use of the tools presented (in the form of videos or web pages), quizzes, and exercises to put into practice the methods presented.

The exercises are based on concrete cases, allowing learners to be in a research situation. 
To carry out these projects, we propose three paths, each using a different technology to write computational documents (Jupyter, Rstudio or Emacs/Org-mode). One of these paths, Jupyter, does not require any installation on the part of the student because we provide each student with a Jupyter workspace integrated into FUN. 

In addition, to encourage the collaborative aspects of the training, a publicly accessible personal workspace has been set up for each student via the Gitlab tool. 

## Documents to be provided :
At the end of the training, you will have to download the certificate of achievement on your personal ADUM space for us to take into account these training hours. 

**Web site**:
https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about
